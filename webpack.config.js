/* eslint-disable */

var path = require('path');
var webpack = require('webpack');

module.exports = {
  entry: {
    common: './src/helpers/client/common.js',
    pagination: './src/helpers/client/pagination.js',
    incomeForm: './src/helpers/client/incomeForm.js',
    incomeList: './src/helpers/client/incomeList.js',
  },

  output: {
    path: path.join(__dirname, 'src/public/js'),
    filename: '[name].bundle.js',
  },

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          presets: ['es2015'],
        },
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },
    ],
  },

  resolve: {
    extensions: ['.js', '.jsx'],
  },
};
