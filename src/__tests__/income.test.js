/* eslint-env jest */
/* eslint-disable no-underscore-dangle */

import { connect, disconnect } from '../helpers/connection';
import { Departments, Incomes, Users } from '../models';
import {
  recordFactory,
  departmentFactory,
  dedicationTypeFactory,
  userFactory
} from '../models/factories';

beforeAll(() => connect());

afterAll(() => disconnect());

describe('Income create', () => {
  let _user;
  let _department;

  const generateRecords = async (times) => {
    let i = 0;

    const records = [];

    while (i < times) {
      i++;

      const record = await recordFactory({ departmentId: _department._id });

      records.push(record);
    }

    return records;
  }

  const createIncomes = async (recordsCount, times) => {
    let i = 0;

    while (i < times) {
      i++;

      await Incomes.createMethod(await generateRecords(recordsCount), _user);
      sleep(3000);
    }
  }

  const sleep = (milliseconds) => {
    const start = new Date().getTime();

    for (let i = 0; i < 1e7; i++) {
      if ((new Date().getTime() - start) > milliseconds){
        break;
      }
    }
  }

  beforeEach(async () => {
    // Creating test data
    _department = await departmentFactory();
    _user = await userFactory({ departmentId: _department._id });
  });

  afterEach(async () => {
    // Clearing test data
    await Users.remove({});
    await Departments.remove({});
    await Incomes.remove({});
  });

  test('Basic', async () => {
    const r1 = await recordFactory({
      departmentId: _department._id,
      groupCode: '19410',
      dedicationTypeId: "5a4c6680829e7409c5ebfc4a",
      amount: 48919,
      accountNumber: 10591,
      accountHolder: 'Chair',
      recipientId: 'recipient',
      printSeparateDocument: false,
    });

    const r2 = await recordFactory({
      departmentId: _department._id,
      groupCode: '18410',
      dedicationTypeId: "5a4c6680829e7409c5ebfc4a",
      amount: 58919,
      accountNumber: 20591,
      accountHolder: 'batamar',
      recipientId: 'recipient',
      printSeparateDocument: true,
    });

    await Incomes.createMethod([r1, r2], _user);

    expect(await Incomes.find().count()).toBe(2);

    const [income1, income2] = await Incomes.find();

    // auto fields
    expect(income1.number).toBe(1);
    expect(income1.createdDate).toEqual(expect.any(Date));
    expect(income1.createdUserId).toEqual(_user._id);
    expect(income1.billNumber).toBe(1);
    expect(income1.status).toBe(1);
    expect(income1.numberOfDocumentPrints).toBe(1);

    expect(income1.departmentId).toEqual(r1.departmentId);
    expect(income1.groupCode).toEqual(r1.groupCode);
    expect(income1.dedicationTypeId.toString()).toBe(r1.dedicationTypeId);
    expect(income1.amount).toBe(r1.amount);
    expect(income1.accountNumber).toBe(r1.accountNumber);
    expect(income1.accountHolder).toBe(r1.accountHolder);
    expect(income1.recipientId).toBe(r1.recipientId);
    expect(income1.printSeparateDocument).toBe(r1.printSeparateDocument);

    // auto fields
    expect(income2.billNumber).toBe(1);
    expect(income2.number).toBe(2);
    expect(income2.createdDate).toEqual(expect.any(Date));
    expect(income2.createdUserId).toEqual(_user._id);
    expect(income2.status).toBe(1);
    expect(income2.numberOfDocumentPrints).toBe(1);

    expect(income2.departmentId).toEqual(r2.departmentId);
    expect(income2.groupCode).toEqual(r2.groupCode);
    expect(income2.dedicationTypeId.toString()).toBe(r2.dedicationTypeId);
    expect(income2.amount).toBe(r2.amount);
    expect(income2.accountNumber).toBe(r2.accountNumber);
    expect(income2.accountHolder).toBe(r2.accountHolder);
    expect(income2.recipientId).toBe(r2.recipientId);
    expect(income2.printSeparateDocument).toBe(r2.printSeparateDocument);
  });

  test('Do not save too often requests', async () => {
    expect(await Incomes.find().count()).toBe(0);

    await Incomes.createMethod(await generateRecords(2), _user);
    await Incomes.createMethod(await generateRecords(2), _user);

    expect(await Incomes.find().count()).toBe(2);
  });

  test('Decent consecutive requests', async () => {
    expect(await Incomes.find().count()).toBe(0);

    await createIncomes(2, 3);

    expect(await Incomes.find().count()).toBe(6);
  }, 10000000);

  test('Multiple', async () => {
    expect(await Incomes.find().count()).toBe(0);

    await createIncomes(2, 10);

    const incomes = await Incomes.find().sort({ number: 1 });

    expect(incomes.length).toBe(20);

    const numbers = [];
    const billNumbers = {};

    for (let income of incomes) {
      // all numbers must be different
      expect(numbers).not.toContain(income.number);

      numbers.push(income.number);

      if (!billNumbers[income.createdDate]) {
        billNumbers[income.createdDate] = [];
      }

      billNumbers[income.createdDate].push(income.billNumber);
    }

    // incomes with same createdDate must have same billNumber
    expect(Object.keys(billNumbers).length).toBe(10);

    for (let key of Object.keys(billNumbers)) {
      const value = billNumbers[key];

      expect(value.length).toBe(2);

      const [billNumber1, billNumber2] = value;

      expect(billNumber1).toBe(billNumber2);
    }

  }, 10000000);

  test('Check billNumber duplications', async () => {
    await createIncomes(1, 1);

    // update database: wrong action
    await Incomes.update({}, { $set: { billNumber: 2 } });

    // create income with different departmentId ========
    const department = await departmentFactory();
    const user = await userFactory({ departmentId: department._id });
    const record = await recordFactory({
      createdDate: new Date('2012-09-12'),
      departmentId: department._id
    });
    await Incomes.createMethod([record], user);

    expect(await Incomes.find().count()).toBe(2);

    // try to create income that will cause duplicated billNumber bug
    await createIncomes(1, 1);

    expect(await Incomes.find().count()).toBe(2);
  }, 10000000);

  test('Calculate runtime variables', async () => {
    await Incomes.calculateRuntimeVariables();

    await createIncomes(2, 4);

    await Incomes.calculateRuntimeVariables();

    const { billNumbers, lastRecordNumber } = Incomes.getInformation();

    expect(lastRecordNumber).toBe(8);
    expect(billNumbers[Incomes.billNumberKey(_department._id)]).toBe(4);
  }, 10000000);

  test('Exactly same income', async () => {
    expect.assertions(4);

    await Incomes.remove({});

    const dedicationType = await dedicationTypeFactory({});

    const doc = {
      from: 'from',
      groupCode: 'groupCode',
      dedicationTypeId: dedicationType._id,
      amount: 1000,
      departmentId: _user.departmentId,
      accountNumber: 1,
      recipientId: 'recipientId'
    }

    expect(await Incomes.find({}).count()).toBe(0);

    await Incomes.createMethod([doc], _user);
    sleep(3000);

    expect(await Incomes.find({}).count()).toBe(1);

    try {
      await Incomes.createMethod([doc], _user);
    } catch (e) {
      expect(e.message).toBe(
        `Хэнээс: ${doc.from}, Зориулалт: ${doc.groupCode}, Дүн: ${doc.amount}, Бүхий мэдээлэл давхардсан байна.` // eslint-disable-line
      );
    }

    // update previous entries created. Same entries can insert in
    // diffrent days
    await Incomes.update({}, { $set: { createdDate: new Date('2000-01-01') } });

    // recreate
    await Incomes.createMethod([doc], _user);

    // must be inserted
    expect(await Incomes.find({}).count()).toBe(2);
  });
});
