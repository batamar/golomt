/*
 * Баримтын route ууд
 */

import { Router } from 'express';
import { Users, Departments, Incomes, DedicationTypes, Recipients } from '../models';
import numberToWord from '../helpers/numberToWord';

const router = Router();

// will call single or multi document types multiple times from this view
router.get('/:incomeId/', async (req, res) => {
  const incomeId = req.params.incomeId;

  const income = await Incomes.findOne({ _id: incomeId });
  const department = await Departments.findOne({ _id: income.departmentId });

  // Only increase bill printed number when supervisor clicked print button in incomes list
  if(req.query.fromLink) {
    await Incomes.increaseDocumentPrintedNumber(incomeId);
  }

  // render with multi document type
  res.render('incomes/documentBase', { incomeId, department });
});

// multi document type
router.get('/multi/:incomeId/', async (req, res) => {
  const printSeparateDocument = Boolean(req.query.separate);

  const income = await Incomes.findOne({ _id: req.params.incomeId });

  income.recipient = await Recipients.findOne({ _id: income.recipientId });
  income.createdUser = await Users.findOne({ _id: income.createdUserId });

  // find sibling incomces
  const siblings = await Incomes.find({
    createdDate: income.createdDate,
    printSeparateDocument,
  });

  if (siblings.length === 0) {
    return res.end();
  }

  const extendedSiblings = [];

  let totalAmount = 0;

  for (let sibling of siblings) {
    const doc = sibling._doc;

    // increase total amount
    totalAmount += doc.amount;

    extendedSiblings.push({
      ...doc,
      dedicationType: await DedicationTypes.findOne({ _id: doc.dedicationTypeId }),
    });
  }

  const totalAmountInWords = numberToWord(totalAmount);

  // render with multi document type
  res.render('incomes/documentMulti', {
    mainIncome: income,
    siblings: extendedSiblings,
    accountHolder: siblings[0].accountHolder,
    totalAmount,
    totalAmountInWords,
  });
});

// single document type
router.get('/single/:incomeId/', async (req, res) => {
  const incomeId = req.params.incomeId;

  const mainIncome = await Incomes.findOne({ _id: incomeId });
  const department = await Departments.findOne({ _id: mainIncome.departmentId });

  mainIncome.recipient = await Recipients.findOne({ _id: mainIncome.recipientId });
  mainIncome.amountInWords = numberToWord(mainIncome.amount);

  return res.render('incomes/documentSingle', { mainIncome, department });
});

export default router;
