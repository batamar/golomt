/*
 * Зориулалт болон дансын route ууд
 */

import { Router } from 'express';
import { Departments, Accounts, DedicationTypes } from '../models';

const router = Router();

// list
router.get('/', async (req, res) => {
  const query = {};

  // only admin can show all incomes
  if (req.user.role !== 'admin') {
    query.departmentId = req.user.departmentId;
  }

  const accounts = await Accounts.find(query).sort({ groupCode: 1 });
  const objects = [];

  for (let account of accounts) {
    const doc = account._doc;

    objects.push({
      ...doc,

      department: await Departments.findOne({
        _id: doc.departmentId,
      }),

      dedicationType: await DedicationTypes.findOne({
        _id: doc.dedicationTypeId,
      }),
    });
  }

  res.render('accounts/list', { objects });
});

const renderGetForm = async (doc, res) => {
  const dedicationTypes = await DedicationTypes.find({});
  const departments = await Departments.find({});

  res.render('accounts/form', { doc, dedicationTypes, departments });
};

const renderPostForm = async (methodName, params, req, res) => {
  const dedicationTypes = await DedicationTypes.find({});
  const departments = await Departments.find({});

  Accounts[methodName](...params)
    .then(() => {
      req.flash('notify', 'Амжилттай хадгаллаа');
      res.redirect('/accounts');
    })
    .catch(e =>
      res.render('accounts/form', {
        errors: e.errors,
        doc: req.body,
        dedicationTypes,
        departments,
      }),
    );
};

const departmentIdParser = req => {
  // only admin can show all incomes
  if (req.user.role !== 'admin') {
    req.body.departmentId = req.user.departmentId;
  }

  return req.body;
};

// create ===========
router.get('/create', (req, res) => {
  renderGetForm({ dedicationTypeId: '', departmentId: '' }, res);
});

router.post('/create', (req, res) => {
  renderPostForm('createMethod', [departmentIdParser(req)], req, res);
});

// update ============
router.get('/update/:_id', async (req, res) => {
  const doc = await Accounts.findOne({ _id: req.params._id });
  renderGetForm(doc, res);
});

router.post('/update/:_id', (req, res) => {
  renderPostForm('updateMethod', [req.params._id, departmentIdParser(req)], req, res);
});

// delete ============
router.get('/delete/:_id', async (req, res) => {
  const doc = await Accounts.findOne({ _id: req.params._id });

  res.render('accounts/delete', { doc });
});

router.post('/delete/:_id', async (req, res) => {
  const doc = await Accounts.findOne({ _id: req.params._id });

  Accounts.deleteMethod(req.params._id)
    .then(() => {
      req.flash('notify', 'Амжилттай устгалаа');
      res.redirect('/accounts');
    })
    .catch(e =>
      res.render('accounts/delete', { errors: e.errors || [{ message: e.message }], doc }),
    );
});

export default router;
