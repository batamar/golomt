import moment from 'moment';
import { Users, Incomes, DedicationTypes, Recipients } from '../models';
import { PER_PAGE } from '../constants';

// build query ==========
export const generateListQuery = filters => {
  const {
    billNumber,
    createdUserId,
    accountNumber,
    accountHolder,
    from,
    groupCode,
    amount,
    date,
  } = filters;

  const query = {};

  if (billNumber) {
    query.billNumber = billNumber;
  }

  if (createdUserId) {
    query.createdUserId = createdUserId;
  }

  if (accountNumber) {
    query.accountNumber = accountNumber;
  }

  if (accountHolder) {
    query.accountHolder = { $regex: `.*${accountHolder.toLowerCase()}.*`, $options: 'i' };
  }

  if (groupCode) {
    query.groupCode = groupCode;
  }

  if (amount) {
    query.amount = Number(amount) || 0;
  }

  if (from) {
    query.from = { $regex: `.*${from.toLowerCase()}.*`, $options: 'i' };
  }

  if (date) {
    const startOfDay = moment(new Date(date)).startOf('day');
    const nextDay = moment(startOfDay).add(1, 'days');

    query.createdDate = { $gte: startOfDay, $lt: nextDay };
  }

  return query;
};

// add dedicationType, recipient objects to incomes objects list
export const extendIncomes = async incomes => {
  const objects = [];

  for (let income of incomes) {
    const doc = income._doc;

    objects.push({
      ...doc,
      createdUser: await Users.findOne({ _id: doc.createdUserId }),
      createdDate: moment(doc.createdDate).format('YYYY-MM-DD HH:mm'),

      dedicationType: await DedicationTypes.findOne({
        _id: doc.dedicationTypeId,
      }),

      recipient: await Recipients.findOne({
        _id: doc.recipientId,
      }),
    });
  }

  return objects;
};

export const getIncomes = async ({ queryString, user }, mongoQuery) => {
  const departmentId = user.departmentId;
  const userId = user._id;
  const userRole = user.role;

  // build query from query string ==========
  mongoQuery = { ...mongoQuery, ...generateListQuery(queryString) };

  // if user in regular incomes list and there is not filter then
  // show only today's incomes
  if (mongoQuery.status === 1 && Object.keys(queryString).length === 0) {
    const startOfDay = moment(new Date()).startOf('day');
    const nextDay = moment(startOfDay).add(1, 'days');

    mongoQuery.createdDate = { $gte: startOfDay, $lt: nextDay };
  }

  // only admin can see all incomes
  if (userRole !== 'admin') {
    mongoQuery.departmentId = departmentId;
  }

  // tellers can see only their incomes
  if (userRole === 'teller') {
    mongoQuery.createdUserId = userId;
  }

  const page = Number(queryString.page || '1');
  const limit = Number(queryString.perPage || PER_PAGE.toString());
  const skip = (page - 1) * limit;

  // find incomes
  const incomes = await Incomes.find(mongoQuery)
    .limit(limit)
    .skip(skip)
    .sort({ [queryString.sortField || 'billNumber']: -1 });

  // sum amout of today's incomes
  const startOfDay = moment().startOf('day');
  const nextDay = moment(startOfDay).add(1, 'days');

  let amountQuery = { departmentId, createdDate: { $gte: startOfDay, $lt: nextDay } };

  // sum amount of filtered incomes
  if (queryString) {
    amountQuery = { ...mongoQuery };
  }

  // calculate sum
  let sumAmount = 0;
  let sumCount = 0;

  const amountIncomes = await Incomes.find(amountQuery);

  for(let income of amountIncomes) {
    sumAmount += income.amount;
    sumCount++;
  }

  return {
    objects: await extendIncomes(incomes),
    users: await Users.find({ departmentId }),
    totalCount: await Incomes.count(mongoQuery),
    billNumberCount: (await Incomes.find(mongoQuery).distinct('createdDate')).length,
    sumAmount,
    sumCount,
  };
};
