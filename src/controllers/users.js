import { Router } from 'express';
import { Users, Departments } from '../models';
import { findUser } from '../helpers/ldap';

const router = Router();

// user list
router.get('/', async (req, res) => {
  const users = await Users.find({});

  const objects = [];

  for (let user of users) {
    const doc = user._doc;

    objects.push({
      ...doc,

      department: await Departments.findOne({
        _id: doc.departmentId,
      }),
    });
  }

  res.render('users/list', { objects });
});

// create ===========
router.get('/create', async (req, res) => {
  const departments = await Departments.find({});

  res.render('users/form', { doc: { departmentId: '' }, departments });
});

const renderPostForm = async (req, res, methodName, params) => {
  const departments = await Departments.find({});

  // fetch user from ldap server using sAMAccountname
  findUser(req.body.sAMAccountName, ({ status, error }) => {
    // user not found
    if (status === 'error') {
      return res.render('users/form', { errors: [error], doc: req.body, departments });
    }

    // user found && try to create new one
    Users[methodName](...params)
      .then(() => {
        req.flash('notify', 'Амжилттай үүсгэлээ');
        res.redirect('/users');
      })

      .catch(e =>
        res.render('users/form', {
          errors: e.errors || [{ message: e.message }],
          doc: req.body,
          departments,
        }),
      );
  });
}

router.post('/create', async (req, res) => {
  renderPostForm(req, res, 'createMethod', [req.body]);
});

// update ============
router.get('/update/:_id', async (req, res) => {
  res.render('users/form', {
    departments: await Departments.find({}),
    doc: await Users.findOne({ _id: req.params._id }),
  });
});

router.post('/update/:_id', async (req, res) => {
  renderPostForm(req, res, 'updateMethod', [req.params._id, req.body]);
});

// delete ============
router.get('/delete/:_id', (req, res) => {
  Users.findOne({ _id: req.params._id }).then(doc => res.render('users/delete', { doc }));
});

router.post('/delete/:_id', async (req, res) => {
  const user = await Users.findOne({ _id: req.params._id });

  Users.deleteMethod(req.params._id)
    .then(() => {
      req.flash('notify', 'Амжилттай устгалаа');
      res.redirect('/users');
    })
    .catch(e =>
      res.render('users/delete', {
        errors: e.errors || [{ message: e.message }],
        doc: user,
      }),
    );
});

export default router;
