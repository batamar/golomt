/*
 * Орлогын route ууд
 */

import { Router } from 'express';
import moment from 'moment';
import { Users, Incomes, DedicationTypes, Accounts, Recipients } from '../models';
import { getIncomes, generateListQuery, extendIncomes } from './incomeHelpers';

const router = Router();

const lastIncomeSort = { createdDate: -1 };

// list
router.get('/', async (req, res, next) => {
  // income list must have at least one date filter
  if (req.path === '/' && Object.keys(req.query).length === 0) {
    res.redirect(`/?date=${new Date().toLocaleDateString()}`);
    return next();
  }

  res.render(
    'incomes/list',
    { ...(await getIncomes(
      { queryString: req.query, user: req.user },
      { status: 1 }))
    }
  );
});

// deleted list
router.get('/incomes/deleted', async (req, res) => {
  res.render(
    'incomes/deletedList',
    { ...(await getIncomes(
      { queryString: req.query, user: req.user },
      { status: -1}))
    }
  );
});

// create ===========
router.get('/incomes/create', async (req, res) => {
  // generate next bill number
  const departmentId = req.user.departmentId;
  const billNumber = await Incomes.getNextBillNumber(departmentId);
  const recipients = await Recipients.find({ departmentId });

  // find last income information ============

  // last income dedication type
  let lastIncomeDtNames = [];

  const mainSelector = { createdUserId: req.user._id };
  const lastIncome = await Incomes.findOne(mainSelector).sort(lastIncomeSort);

  if (lastIncome) {
    const selector = { ...mainSelector, createdDate: lastIncome.createdDate };
    const lastIncomes = await Incomes.find(selector).sort(lastIncomeSort);

    for(let income of lastIncomes) {
      const dt = await DedicationTypes.findOne({
        _id: income.dedicationTypeId
      });

      if (!lastIncomeDtNames.includes(dt.name)) {
        lastIncomeDtNames.push(dt.name);
      }
    }
  }

  // render form
  res.render('incomes/create', {
    doc: {
      createdDate: new Date().toLocaleDateString(),
      billNumber,
    },
    lastIncomeDtNames: lastIncomeDtNames.join(', '),
    recipients,
  });
});

router.post('/incomes/create', async (req, res) => {
  const records = JSON.parse(req.body.records);

  // create income
  Incomes.createMethod(records, req.user).then((lastIncome) => {
    if (lastIncome) {
      req.flash('notify', 'Амжилттай хадгаллаа');
      res.json({ status: 'ok', incomeId: lastIncome._id });
    }
  })

  .catch(e =>
    res.json({ status: 'error', error: e.message })
  );
});

// delete ============
router.get('/incomes/delete/:_id', async (req, res) => {
  const doc = await Incomes.findOne({ _id: req.params._id });

  res.render('incomes/delete', { doc });
});

router.post('/incomes/delete/:_id', async (req, res) => {
  const { params, body, user } = req;

  Incomes.deleteMethod({ _id: params._id, reason: body.reason, user })
    .then(() => {
      req.flash('notify', 'Амжилттай устгалаа');
      res.redirect('/');
    })
    .catch(async e => {
      const doc = await Incomes.findOne({ _id: req.params._id });

      res.render('incomes/delete', { errors: e.errors || [e], doc });
    });
});

// helpers ===========
router.get('/incomes/load-accounts/:groupCode/', async (req, res) => {
  const accounts = await Accounts.find({
    departmentId: req.user.departmentId,
    groupCode: req.params.groupCode,
  });

  const extendedAccounts = [];

  for (let account of accounts) {
    extendedAccounts.push({
      ...account._doc,
      dedicationType: await DedicationTypes.findOne({ _id: account.dedicationTypeId }),
    });
  }

  res.send(extendedAccounts);
});

router.get('/incomes/load-last-income', async (req, res) => {
  const lastIncome = await Incomes.findOne({
      createdUserId: req.user._id }).sort(lastIncomeSort);

  return res.send(lastIncome);
});

// kind of report
router.get('/incomes/print/:status', async (req, res) => {
  const status = req.params.status;

  // build query from query string ==========
  const mongoQuery = generateListQuery(req.query);

  mongoQuery.status = status;

  // tellers can see only their incomes
  if (req.user.role === 'teller') {
    mongoQuery.createdUserId = req.user._id;
  }

  // find incomes
  const incomes = await Incomes.find(mongoQuery);

  let sumCount = 0;
  let sumAmount = 0;

  for (let income of incomes) {
    sumCount++;
    sumAmount += income.amount;
  }

  // render
  res.render(
    'incomes/report',
    {
      objects: await extendIncomes(incomes),
      sumCount,
      sumAmount,
      status,
    }
  );
});

// teller report
router.get('/incomes/teller-report', async (req, res) => {
  const date = req.query.date ? new Date(req.query.date) : new Date();
  const users = await Users.find({ role: 'teller' });

  // find only given's incomes
  const startOfDay = moment(date).startOf('day');
  const nextDay = moment(startOfDay).add(1, 'days');

  const response = [];

  let totalRecordsCount = 0;
  let totalRecordsBillNumberCount = 0;

  for (let user of users) {
    const query = {
      createdDate: { $gte: startOfDay, $lt: nextDay },
      createdUserId: user._id,
      status: 1,
    };

    const recordsCount = await Incomes.find(query).count();
    const recordsBillNumberCount = (await Incomes.find(query).distinct('createdDate')).length;

    totalRecordsCount += recordsCount;
    totalRecordsBillNumberCount += recordsBillNumberCount;

    response.push({
      user,
      recordsCount,
      recordsBillNumberCount,
    });
  }

  // render
  res.render('incomes/tellerReport', {
    date,
    response,
    totalRecordsCount,
    totalRecordsBillNumberCount,
  });
});

// invalid entries
// TODO remove after stable
router.get('/incomes/invalid-entries', async (req, res) => {
  const incomes = await Incomes.find({});

  const duplicatedItems = [];

  for (let income of incomes) {
    const count = await Incomes.find({
      _id: { $ne: income._id },
      number: income.number,
    }).count();

    if (count > 0) {
      duplicatedItems.push({
        ...income._doc,
        createdUser: await Users.findOne({ _id: income.createdUserId }),
        createdDate: moment(income.createdDate).format('YYYY-MM-DD HH:mm'),

        dedicationType: await DedicationTypes.findOne({
          _id: income.dedicationTypeId,
        }),

        recipient: await Recipients.findOne({
          _id: income.recipientId,
        }),
      });
    }
  }

  // render
  res.render('incomes/invalidReport', { duplicatedItems });
});

router.get('/get-information', async (req, res) => {
  const info = await Incomes.getInformation({});

  res.json(info);
});

export default router;
