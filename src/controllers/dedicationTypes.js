/*
 * Зориулалтын төрлийн route ууд
 */

import { Router } from 'express';
import { DedicationTypes } from '../models';

const router = Router();

// list
router.get('/', async (req, res) => {
  const objects = await DedicationTypes.find({});

  res.render('dedicationTypes/list', { objects });
});

// create ===========
router.get('/create', (req, res) => {
  res.render('dedicationTypes/form', { doc: {} });
});

router.post('/create', (req, res) => {
  DedicationTypes.createMethod(req.body)
    .then(() => res.redirect('/dedication-types'))
    .catch(e => res.render('dedicationTypes/form', { errors: e.errors, doc: req.body }));
});

// update ============
router.get('/update/:_id', async (req, res) => {
  const doc = await DedicationTypes.findOne({ _id: req.params._id });

  res.render('dedicationTypes/form', { doc, account: {} });
});

router.post('/update/:_id', (req, res) => {
  DedicationTypes.updateMethod(req.params._id, req.body)
    .then(() => res.redirect('/dedication-types'))
    .catch(e => res.render('dedicationTypes/form', { errors: e.errors, doc: req.body }));
});

// delete ============
router.get('/delete/:_id', async (req, res) => {
  const doc = await DedicationTypes.findOne({ _id: req.params._id });

  res.render('dedicationTypes/delete', { doc });
});

router.post('/delete/:_id', async (req, res) => {
  const doc = await DedicationTypes.findOne({ _id: req.params._id });

  DedicationTypes.deleteMethod(req.params._id)
    .then(() => res.redirect('/dedication-types'))
    .catch(e =>
      res.render('dedicationTypes/delete', { errors: e.errors || [{ message: e.message }], doc }),
    );
});

export default router;
