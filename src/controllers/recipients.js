/*
 * Хүлээн авагчийн route ууд
 */

import { Router } from 'express';
import { Departments, Recipients } from '../models';

const router = Router();

// list
router.get('/', async (req, res) => {
  const recipients = await Recipients.find({});
  const objects = [];

  for (let recipient of recipients) {
    const doc = recipient._doc;

    objects.push({
      ...doc,

      department: await Departments.findOne({
        _id: doc.departmentId,
      }),
    });
  }

  res.render('recipients/list', { objects });
});

const renderGetForm = async (doc, res) => {
  const departments = await Departments.find({});

  res.render('recipients/form', { doc, departments });
};

const renderPostForm = async (methodName, params, req, res) => {
  const departments = await Departments.find({});

  Recipients[methodName](...params)
    .then(() => {
      req.flash('notify', 'Амжилттай хадгаллаа');
      res.redirect('/recipients');
    })
    .catch(e =>
      res.render('recipients/form', {
        errors: e.errors,
        doc: req.body,
        departments,
      }),
    );
};

// create ===========
router.get('/create', (req, res) => {
  renderGetForm({ dedicationTypeId: '', departmentId: '' }, res);
});

router.post('/create', (req, res) => {
  renderPostForm('createMethod', [req.body], req, res);
});

// update ============
router.get('/update/:_id', async (req, res) => {
  const doc = await Recipients.findOne({ _id: req.params._id });
  renderGetForm(doc, res);
});

router.post('/update/:_id', (req, res) => {
  renderPostForm('updateMethod', [req.params._id, req.body], req, res);
});

// delete ============
router.get('/delete/:_id', async (req, res) => {
  const doc = await Recipients.findOne({ _id: req.params._id });

  res.render('recipients/delete', { doc });
});

router.post('/delete/:_id', async (req, res) => {
  const doc = await Recipients.findOne({ _id: req.params._id });

  Recipients.deleteMethod(req.params._id)
    .then(() => {
      req.flash('notify', 'Амжилттай устгалаа');
      res.redirect('/recipients');
    })
    .catch(e =>
      res.render('recipients/delete', { errors: e.errors || [{ message: e.message }], doc }),
    );
});

export default router;
