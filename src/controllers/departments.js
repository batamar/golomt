/*
 * Салбарын route ууд
 */

import { Router } from 'express';
import { Departments, Recipients } from '../models';

const router = Router();

// list
router.get('/', async (req, res) => {
  const departments = await Departments.find({});
  const objects = [];

  for (let department of departments) {
    const doc = department._doc;

    objects.push({
      ...doc,

      // add recipient object to every item
      defaultRecipient: await Recipients.findOne({
        _id: doc.defaultRecipientId,
      }),
    });
  }

  res.render('departments/list', { objects });
});

const renderGetForm = async (doc, res) => {
  const recipients = await Recipients.find({});

  res.render('departments/form', { doc, recipients });
};

const renderPostForm = async (methodName, params, req, res) => {
  const recipients = await Recipients.find({});

  Departments[methodName](...params)
    .then(() => {
      req.flash('notify', 'Амжилттай хадгаллаа');
      res.redirect('/departments');
    })
    .catch(e =>
      res.render('departments/form', {
        errors: e.errors,
        doc: req.body,
        recipients,
      })
    );
};

// create ===========
router.get('/create', (req, res) => {
  renderGetForm({ defaultRecipientId: '' }, res);
});

router.post('/create', (req, res) => {
  renderPostForm('createMethod', [req.body], req, res);
});

// update ============
router.get('/update/:_id', async (req, res) => {
  const doc = await Departments.findOne({ _id: req.params._id });
  renderGetForm(doc, res);
});

router.post('/update/:_id', (req, res) => {
  renderPostForm('updateMethod', [req.params._id, req.body], req, res);
});

// delete ============
router.get('/delete/:_id', async (req, res) => {
  const doc = await Departments.findOne({ _id: req.params._id });

  res.render('departments/delete', { doc });
});

router.post('/delete/:_id', async (req, res) => {
  const doc = await Departments.findOne({ _id: req.params._id });

  Departments.deleteMethod(req.params._id)
    .then(() => {
      req.flash('notify', 'Амжилттай устгалаа');
      res.redirect('/departments');
    })
    .catch(e =>
      res.render('departments/delete', { errors: e.errors || [{ message: e.message }], doc }),
    );
});

export default router;
