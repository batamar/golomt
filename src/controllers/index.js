import { Router } from 'express';
import departments from './departments';
import dedicationTypes from './dedicationTypes';
import recipients from './recipients';
import accounts from './accounts';
import incomes from './incomes';
import document from './document';
import users from './users';

const router = Router();

router.use('/incomes/document', document);
router.use('/', incomes);
router.use('/departments', departments);
router.use('/dedication-types', dedicationTypes);
router.use('/recipients', recipients);
router.use('/accounts', accounts);
router.use('/users', users);

// role descriptions
router.get('/role-descriptions', async (req, res) => {
  res.render('users/roleDescriptions');
});

export default router;
