import { Users } from '../models';
import { createUserSession } from '../helpers/auth';

export const auth = (req, res, next) => {
  const sess = req.session;
  const path = req.path;

  if (path === '/check-ldap' || path === '/get-information' || path === '/login' || path === '/logout') {
    return next();
  }

  // if previously loggedin then
  if (sess && sess.user) {
    // find user by sam account
    Users.findOne({ sAMAccountName: req.session.user.sAMAccountName }).then(user => {
      if (!user.departmentId) {
        return res.redirect('/login');
      }

      if (user) {
        // assign req.user
        createUserSession(req, res, user);
      }

      const role = user.role;
      const method = req.method;

      if (role === 'inactive') {
        return res.redirect('/login');
      }

      // teller permissions
      if (
        role === 'teller' &&
        !(
          path === '/incomes/create' ||
          path === '/' ||
          path.includes('/incomes/load-accounts') ||
          path.includes('/incomes/load-last-income') ||
          path.includes('/incomes/deleted') ||
          path.includes('/incomes/print') ||
          path === '/accounts' ||
          path.includes('/incomes/document')
        )
      ) {
        return res.redirect('/');
      }

      // only teller can create income
      if (role !== 'teller' && path === '/incomes/create') {
        return res.redirect('/');
      }

      // supervisor can not manage department or user
      if (
        role === 'supervisor' &&
        (path.includes('department') || path.includes('users')) &&
        (method == 'POST' || method == 'DELETE')
      ) {
        return res.redirect('/');
      }

      next();
    });

    // not previoulsy logged
  } else {
    // if user is not already in login page then redirect to it
    if (req.path !== '/login') {
      res.redirect('/login');

      // otherwise do nothing
    } else {
      next();
    }
  }
};

export const requireAdmin = (req, res, next) => {
  if (req.user && req.user.role === 'admin') {
    next();
  } else {
    res.redirect('/');
  }
};
