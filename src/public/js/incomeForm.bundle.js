/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var IncomeForm = exports.IncomeForm = function () {
  function IncomeForm(initialRecords) {
    _classCallCheck(this, IncomeForm);

    this.records = initialRecords;

    this.handleEvents();
    this.handleShortcuts();
  }

  _createClass(IncomeForm, [{
    key: 'handleEvents',
    value: function handleEvents() {
      var _this = this;

      // remove record
      $(document).on('click', '.remove-record', function (e) {
        return _this.removeRecord($(e.target).attr('group-code'));
      });

      // accounts loader
      $('input[name="groupCode"]').keydown(function (e) {
        if (e.keyCode === 13) {
          e.preventDefault();

          // fetch accounts by group code
          _this.loadAccounts(e.target.value);

          return false;
        }
      });

      $('input[name="from"]').keydown(function (e) {
        if (e.keyCode === 13) {
          e.preventDefault();

          $('input[name="groupCode"]').focus();

          return false;
        }
      });

      $('.dismiss').click(function () {
        window.location.href = '/';
      });

      // form submit
      $('form').submit(function (e) {
        _this.formSubmit(e);
      });
    }
  }, {
    key: 'handleShortcuts',
    value: function handleShortcuts() {
      $(document).keydown(function (e) {
        switch (e.keyCode) {
          // f4
          case 115:
            window.location.href = '/';
            break;

          // f5
          case 116:
            e.preventDefault();
            break;

          // f7
          case 118:
            $('form').submit();
            break;

          // f9
          case 120:
            fetch('/incomes/load-last-income', { credentials: 'include' }).then(function (res) {
              return res.json();
            }).then(function (income) {
              $('input[name="from"]').val(income.from);
              $('input[name="groupCode"]').focus();
            }).catch(function (e) {
              console.log(e); // eslint-disable-line
            });
            break;

          default:
        }
      });
    }
  }, {
    key: 'formSubmit',
    value: function formSubmit(e) {
      e.preventDefault();

      // if there is no record then do nothing
      if (this.records.length === 0) {
        return false;
      }

      var updatedRecords = [];

      // calculate each record's amounts from input
      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = this.records[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var record = _step.value;

          updatedRecords.push(_extends({}, record, {
            amount: parseInt($('#amount-' + record._id).val())
          }));
        }

        // disable submit
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator.return) {
            _iterator.return();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }

      $('[type="submit"]').attr('disabled', 'disabled');

      fetch('/incomes/create', {
        method: 'post',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        credentials: 'include',
        body: 'records=' + JSON.stringify(updatedRecords)
      }).then(function (response) {
        return response.json();
      }).then(function (_ref) {
        var status = _ref.status,
            error = _ref.error,
            incomeId = _ref.incomeId;

        // enable submit
        $('[type="submit"]').removeAttr('disabled');

        if (status === 'ok') {
          window.open('/incomes/document/' + incomeId);

          window.location.href = '/incomes/create';
        }

        if (status === 'error') {
          alert(error);
        }
      }).catch(function (e) {
        console.log(e); // eslint-disable-line
      });

      return false;
    }
  }, {
    key: 'loadAccounts',
    value: function loadAccounts(code) {
      var _this2 = this;

      if (!code) {
        return false;
      }

      // if already exists then do not insert duplicated item
      if (this.records.find(function (record) {
        return record.groupCode === code;
      })) {
        return false;
      }

      fetch('/incomes/load-accounts/' + code, { credentials: 'include' }).then(function (res) {
        return res.json();
      }).then(function (accounts) {
        _this2.addRecord(accounts || []);

        // clear group code value after load records
        $('[name="groupCode"]').val('');
      }).catch(function (e) {
        console.log(e); // eslint-disable-line
      });
    }
  }, {
    key: 'addRecord',
    value: function addRecord(accounts) {
      var _this3 = this;

      var _getFormValues = this.getFormValues(),
          status = _getFormValues.status,
          doc = _getFormValues.doc,
          message = _getFormValues.message;

      if (status === 'error') {
        return alert(message);
      }

      accounts.forEach(function (account) {
        _this3.records.push(_extends({}, account, doc));
      });

      this.renderRecords();

      return 'success';
    }

    /*
     * validate form or return doc
     */

  }, {
    key: 'getFormValues',
    value: function getFormValues() {
      var groupCode = $('[name="groupCode"]').val();
      var from = $('[name="from"]').val();
      var recipientId = $('[name="recipientId"]').val();

      if (!groupCode) {
        return {
          status: 'error',
          message: 'Зориулалтын төрөл сонгоно уу'
        };
      }

      if (!from) {
        return {
          status: 'error',
          message: 'Хэнээс талбарын мэдээллийг бөглөнө үү'
        };
      }

      if (!recipientId) {
        return {
          status: 'error',
          message: 'Хүлээн авагчийн мэдээллийг бөглөнө үү'
        };
      }

      // form is valid then return doc
      return {
        status: 'ok',
        doc: {
          billNumber: $('[name="billNumber"]').val(),
          from: from,
          recipientId: recipientId,
          groupCode: groupCode
        }
      };
    }
  }, {
    key: 'removeRecord',
    value: function removeRecord(groupCode) {
      this.records = this.records.filter(function (record) {
        return record.groupCode !== groupCode;
      });
      this.renderRecords();
    }
  }, {
    key: 'calculateTotal',
    value: function calculateTotal() {
      var total = 0;

      $('input.amount').each(function () {
        total += parseInt($(this).val());
      });

      $('#total-amount').html(total.toLocaleString());
    }
  }, {
    key: 'renderRecords',
    value: function renderRecords() {
      var _this4 = this;

      // clear previous data
      $('#records tbody').html('');

      this.records.forEach(function (record) {
        $('#records tbody').append('\n        <tr>\n          <td>\n            ' + record.billNumber + '\n          </td>\n          <td>\n            ' + record.from + '\n          </td>\n          <td>\n            ' + record.groupCode + '\n          </td>\n          <td>\n            ' + record.dedicationType.name + '\n          </td>\n          <td>\n            ' + record.accountNumber + '\n          </td>\n          <td>\n            <input class=\'amount\' id=\'amount-' + record._id + '\' value=\'' + record.amount + '\' />\n          </td>\n          <td>\n            <button\n              type=\'button\'\n              class=\'remove-record btn btn-xs btn-danger\'\n              group-code=\'' + record.groupCode + '\'>\n              x\n            </button>\n          </td>\n        </tr>\n        ');

        // display with thousands seperator
        $('#records tbody tr:last-child .amount').divide();

        $('#records tbody tr:last-child .amount').keyup(function () {
          _this4.calculateTotal();
        });
      });

      this.calculateTotal();
    }
  }]);

  return IncomeForm;
}();

new IncomeForm([]);

/***/ })

/******/ });