/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var getQueryParam = exports.getQueryParam = function getQueryParam(name) {
  var url = window.location.href;

  name = name.replace(/[[\]]/g, '\\$&');

  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);

  if (!results) return null;

  if (!results[2]) return '';

  return decodeURIComponent(results[2].replace(/\+/g, ' '));
};

var setQueryParam = exports.setQueryParam = function setQueryParam(key, val) {
  var url = window.location.href;
  var regex = RegExp('([?&]' + key + '(?=[=&#]|$)[^#&]*|(?=#|$))');

  window.location.href = url.replace(regex, '&' + key + '=' + encodeURIComponent(val)).replace(/^([^?&]+)&/, '$1?');
};

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _common = __webpack_require__(0);

var _constants = __webpack_require__(2);

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// pages calculation
var generatePages = function generatePages(pageCount, currentPage) {
  var w = 4;

  var pages = _.range(1, pageCount + 1);
  var diff = void 0;
  var first = pages.slice(0, w);

  var last = pages.slice(-w);

  var currentStart = currentPage - 1 - w;

  if (currentStart < 0) {
    currentStart = 0;
  }

  var currentEnd = currentPage - 1 + w;

  if (currentEnd < 0) {
    currentEnd = 0;
  }

  var current = pages.slice(currentStart, currentEnd);

  pages = [];

  if (_.intersection(first, current).length === 0) {
    pages = pages.concat(first);
    diff = current[0] - _.last(first);

    if (diff === 2) {
      pages.push(current[0] - 1);
    } else if (diff !== 1) {
      pages.push(null);
    }

    pages = pages.concat(current);
  } else {
    pages = _.union(first, current);
  }

  if (_.intersection(current, last).length === 0) {
    diff = last[0] - _.last(pages);

    if (diff === 2) {
      pages.push(last[0] - 1);
    } else if (diff !== 1) {
      pages.push(null);
    }

    pages = pages.concat(last);
  } else {
    diff = _.difference(last, current);
    pages = pages.concat(diff);
  }

  return pages;
};

// change url page parameter
var goto = function goto(page) {
  (0, _common.setQueryParam)('page', page);
};

// per page

var Page = function () {
  function Page(props) {
    _classCallCheck(this, Page);

    this.props = props;

    this.handleEvents();
  }

  _createClass(Page, [{
    key: 'handleEvents',
    value: function handleEvents() {
      var _this = this;

      $(document).on('click', '[key="page-' + this.props.key + '"]', function () {
        goto(_this.props.page);
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          key = _props.key,
          currentPage = _props.currentPage,
          page = _props.page;


      if (page) {
        var className = '';

        if (page === currentPage) {
          className += ' active disabled';
        }

        return '\n        <li class="' + className + '" key="page-' + key + '">\n          <a href=\'#\'>' + page + '</a>\n        </li>\n      ';
      }

      return '\n      <li class=\'disabled\'>\n        <span>...</span>\n      </li>\n    ';
    }
  }]);

  return Page;
}();

var Pagination = function () {
  function Pagination(_ref) {
    var count = _ref.count;

    _classCallCheck(this, Pagination);

    var currentPage = Number((0, _common.getQueryParam)('page') || 1);
    var perPage = Number((0, _common.getQueryParam)('perPage') || _constants.PER_PAGE);

    var totalPagesCount = parseInt(count / perPage) + 1;

    if (count % perPage === 0) {
      totalPagesCount -= 1;
    }

    // calculate page numbers
    var pages = generatePages(totalPagesCount, currentPage);

    this.props = {
      currentPage: currentPage,
      isPaginated: totalPagesCount > 1,
      totalPagesCount: totalPagesCount,
      pages: pages
    };

    this.handleEvents();
  }

  _createClass(Pagination, [{
    key: 'handleEvents',
    value: function handleEvents() {
      var _props2 = this.props,
          totalPagesCount = _props2.totalPagesCount,
          currentPage = _props2.currentPage;


      $(document).on('click', 'a.prev', function (e) {
        e.preventDefault();

        var page = currentPage - 1;

        if (page > 0) {
          goto(page);
        }
      });

      $(document).on('click', 'a.next', function (e) {
        e.preventDefault();

        var page = currentPage + 1;

        if (page <= totalPagesCount) {
          goto(page);
        }
      });
    }
  }, {
    key: 'renderBar',
    value: function renderBar() {
      var _props3 = this.props,
          totalPagesCount = _props3.totalPagesCount,
          pages = _props3.pages,
          currentPage = _props3.currentPage,
          isPaginated = _props3.isPaginated;


      if (isPaginated) {
        var prevClass = '';
        var nextClass = '';

        if (currentPage <= 1) {
          prevClass = 'disabled';
        }

        if (currentPage >= totalPagesCount) {
          nextClass = 'disabled';
        }

        var renderedPages = '';

        pages.forEach(function (page, index) {
          var pageObject = new Page({ currentPage: currentPage, page: page, key: index });

          renderedPages += pageObject.render();
        });

        return '\n        <nav>\n          <ul class=\'pagination\'>\n            <li>\n              <a href=\'\' class="' + prevClass + ' prev"><</a>\n            </li>\n\n            ' + renderedPages + '\n\n            <li>\n              <a href=\'#\' class="' + nextClass + ' next">></a>\n            </li>\n          </ul>\n        </nav>\n      ';
      }

      return '';
    }
  }, {
    key: 'render',
    value: function render() {
      return '\n      <div class=\'pagination-container\'>\n        <div class=\'pull-right\'>\n          ' + this.renderBar() + '\n        </div>\n\n        <div class=\'clearfix\' />\n      </div>\n    ';
    }
  }]);

  return Pagination;
}();

exports.default = Pagination;

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var PER_PAGE = exports.PER_PAGE = 50;

/***/ })
/******/ ]);