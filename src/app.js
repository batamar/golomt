import path from 'path';
import dotenv from 'dotenv';
import express from 'express';
import session from 'client-sessions';
import bodyParser from 'body-parser';
import passport from 'passport';
import LdapStrategy from 'passport-ldapauth';
import flash from 'express-flash-messages';
import controllers from './controllers';
import { Incomes, Users } from './models';
import { auth } from './middlewares/auth';
import { createUserSession } from './helpers/auth';
import { connect } from './helpers/connection';
import { checkLdap, getOptions as getLdapOptions } from './helpers/ldap';


// load environment variables
dotenv.config();

// connect to mongo database
connect();

const app = express();

const { PORT } = process.env;

// passport middleware
passport.use(new LdapStrategy({ server: getLdapOptions() }, (user, done) => {
  return done(null, user);
}));

app.use(passport.initialize());

// body parser middleware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.set('view engine', 'pug');
app.set('views', __dirname + '/views');

// serve static files
app.use('/static', express.static(path.join(__dirname, '/public')));

// session
app.use(session({
  cookieName: 'session',
  secret: 'afjdkfjalsjfsdjfksjfijiwrwr',
  duration: 30 * 60 * 6000,
  activeDuration: 5 * 60 * 1000,
}));

// flash messages
app.use(flash());

// check login or add user object to request
app.use(auth);

// routes
app.use(controllers);

// try to get all ldap users
app.get('/check-ldap', (req, res) => {
  checkLdap(req, res);
});

app.get('/login', (req, res) => {
  res.render('login');
});

// login against ldap server
app.post('/login', (req, res, next) => {
  passport.authenticate('ldapauth', { session: false }, async (err, user) => {
    if (err) {
      return res.render('login', { error: 'Нэвтрэх үйлдэл амжилтгүй боллоо' });
    }

    // Generate a JSON response reflecting authentication status
    if (!user) {
      return res.render('login', { error: 'Нэвтрэх үйлдэл амжилтгүй боллоо' });
    }

    // try to find user from database
    const dbUser = await Users.findOne({ sAMAccountName: user.sAMAccountName });

    if (!dbUser) {
      return res.render('login', { error: 'Нэвтрэх үйлдэл амжилтгүй боллоо' });
    }

    if (dbUser.role === 'inactive') {
      return res.render('login', { error: 'Идэвхгүй хэрэглэгч' });
    }

    // save user to session
    createUserSession(req, res, user);

    return res.redirect('/');
  })(req, res, next);
});

app.get('/logout', function(req, res) {
  if (req.session) {
    req.session.reset();
  }

  res.redirect('/');
});

app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}...`); // eslint-disable-line

  Incomes.calculateRuntimeVariables();
});
