const capitalize = (str) => {
  const firstLetter = str.substring(0, 1);
  const remainingLetters = str.substring(1);

  return `${firstLetter.toUpperCase()}${remainingLetters.toLowerCase()}`;
}

export const generateShortName = (user) => {
  let shortName = user.sAMAccountName;

  if (user.firstName && user.lastName) {
    shortName = capitalize(user.lastName.substring(0, 1)) + '.'
      + capitalize(user.firstName);
  }

  return shortName;
};

export const createUserSession = (req, res, user) => {
  const cleanUser = {
    _id: user._id,
    firstName: user.firstName,
    lastName: user.lastName,
    sAMAccountName: user.sAMAccountName,
    role: user.role,
    departmentId: user.departmentId,
  };

  cleanUser.shortName = generateShortName(user);

  req.session.user = cleanUser;
  req.user = cleanUser;
  res.locals.user = cleanUser;
};
