/* eslint-disable max-len */

const range = number => {
  const list = [];

  let i = 0;

  while (i < number) {
    list.push(i);

    i++;
  }

  return list;
};

const numbersToWords = number => {
  let numberInWords = '';

  const [ten_n, hundred_n, thousand_n] = [10, 100, 1000];
  const [zero_n, one_n, two_n, three_n, four_n, five_n, six_n, seven_n, eight_n, nine_n] = [
    0,
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
  ];
  const [ten_w, hundred_w, thousand_w, million_w, billion_w] = [
    'арав',
    'зуу',
    'мянга',
    'сая',
    'тэрбум',
  ];
  const [ten_w2, hundred_w2, thousand_w2, million_w2, billion_w2] = [
    'арван',
    'зуун',
    'мянга',
    'сая',
    'тэрбум',
  ];
  const tens_w = ['ноль10', 'арав', 'хорь', 'гуч', 'дөч', 'тавь', 'жар', 'дал', 'ная', 'ер'];
  const tens_w2 = [
    'нолийн10',
    'арван',
    'хорин',
    'гучин',
    'дөчин',
    'тавин',
    'жаран',
    'далан',
    'наян',
    'ерөн',
  ];
  const [zero_w, one_w, two_w, three_w, four_w, five_w, six_w, seven_w, eight_w, nine_w] = [
    'ноль',
    'нэг',
    'хоёр',
    'гурав',
    'дөрөв',
    'тав',
    'зургаа',
    'долоо',
    'найм',
    'ес',
  ];
  const [
    zero_w2,
    one_w2,
    two_w2,
    three_w2,
    four_w2,
    five_w2,
    six_w2,
    seven_w2,
    eight_w2,
    nine_w2,
  ] = ['нолийн', 'нэг', 'хоёр', 'гурван', 'дөрвөн', 'таван', 'зургаан', 'долоон', 'найман', 'есөн'];
  const [
    zero_w3,
    one_w3,
    two_w3,
    three_w3,
    four_w3,
    five_w3,
    six_w3,
    seven_w3,
    eight_w3,
    nine_w3,
  ] = [
    'нолийн',
    'нэгэн',
    'хоёр',
    'гурван',
    'дөрвөн',
    'таван',
    'зургаан',
    'долоон',
    'найман',
    'есөн',
  ];

  const big_number_words = [ten_w, hundred_w, thousand_w, million_w, billion_w];
  const big_number_words2 = [ten_w2, hundred_w2, thousand_w2, million_w2, billion_w2];

  const numbers_and_words = {};

  numbers_and_words['numbers'] = [
    zero_n,
    one_n,
    two_n,
    three_n,
    four_n,
    five_n,
    six_n,
    seven_n,
    eight_n,
    nine_n,
  ];
  numbers_and_words['numbers_words'] = [
    zero_w,
    one_w,
    two_w,
    three_w,
    four_w,
    five_w,
    six_w,
    seven_w,
    eight_w,
    nine_w,
  ];
  numbers_and_words['numbers_words2'] = [
    zero_w2,
    one_w2,
    two_w2,
    three_w2,
    four_w2,
    five_w2,
    six_w2,
    seven_w2,
    eight_w2,
    nine_w2,
  ];
  numbers_and_words['numbers_words3'] = [
    zero_w3,
    one_w3,
    two_w3,
    three_w3,
    four_w3,
    five_w3,
    six_w3,
    seven_w3,
    eight_w3,
    nine_w3,
  ];

  const get_1_999 = (number, raw_number, use_w3 = false) => {
    let result = '';

    // 1-9
    if (number % ten_n) {
      if (use_w3) {
        if (raw_number < 10000) {
          result = numbers_and_words['numbers_words2'][number % ten_n] + result;
        } else {
          if (number % 100 > 10 && number % 10 === 1) {
            result = numbers_and_words['numbers_words3'][number % ten_n] + result;
          } else {
            result = numbers_and_words['numbers_words2'][number % ten_n] + result;
          }
        }
      } else {
        result = numbers_and_words['numbers_words'][number % ten_n] + result;
      }
    }

    // 10-99
    if (number !== 0) {
      if (number % 100 !== 0 && number % ten_n === 0) {
        if (use_w3) {
          result = `${tens_w2[Math.floor(number % hundred_n / ten_n)]}${result}`;
        } else {
          result = tens_w[Math.floor(number % hundred_n / ten_n)] + result;
        }
      } else if (number % 100 != 0 && Math.floor(number % 100 / 10) != 0 && number > ten_n) {
        if (result != '') {
          result = `${tens_w2[Math.floor(number % hundred_n / ten_n)]} ${result}`;
        } else {
          result = `${tens_w2[Math.floor(number % hundred_n / ten_n)]}`;
        }
      }
    }

    // 100-999
    if (number % thousand_n % hundred_n == 0) {
      if (result != '') {
        result = `${numbers_and_words['numbers_words'][
          Math.floor(number % thousand_n / hundred_n)
        ]} ${hundred_w2} ${result}`;
      } else if (Math.floor(number % thousand_n / hundred_n) !== 0) {
        if (use_w3) {
          result = `${numbers_and_words['numbers_words2'][
            Math.floor(number % thousand_n / hundred_n)
          ]} ${hundred_w2}`;
        } else {
          result = `${numbers_and_words['numbers_words2'][
            Math.floor(number % thousand_n / hundred_n)
          ]} ${hundred_w}`;
        }
      }
    } else if (number % thousand_n > hundred_n) {
      if (result != '') {
        result = `${numbers_and_words['numbers_words2'][
          Math.floor(number % thousand_n / hundred_n)
        ]} ${hundred_w2} ${result}`;
      } else {
        result = `${numbers_and_words['numbers_words2'][
          Math.floor(number % thousand_n / hundred_n)
        ]} ${hundred_w2}`;
      }
    }

    return result;
  };

  // 0
  if (number == 0) {
    return numbers_and_words['numbers_words3'][0];
  }

  // 10<=x<=999
  numberInWords = get_1_999(number, number);

  // 1000<=x<=999999
  if (number % 1000000 > 999 && number % 1000000 < 1000000) {
    if (numberInWords != '') {
      numberInWords = `${get_1_999(
        Math.floor(number / 1000),
        number,
        true,
      )} ${thousand_w} ${numberInWords}`;
    } else {
      numberInWords = `${get_1_999(Math.floor(number / 1000), number, true)} ${thousand_w}`;
    }
  }

  // 1000000<=x<=999999999
  if (number % 1000000000 > 999999 && number % 1000000000 < 1000000000) {
    if (numberInWords != '') {
      numberInWords = `${get_1_999(
        Math.floor(number / 1000000),
        number,
        true,
      )} ${million_w} ${numberInWords}`;
    } else {
      numberInWords = `${get_1_999(Math.floor(number / 1000000), number, true)} ${million_w}`;
    }
  }

  // 1000000000<=x<=999999999999
  if (number % 1000000000000 > 999999999 && number % 1000000000000 < 1000000000000) {
    if (numberInWords != '') {
      numberInWords = `${get_1_999(
        Math.floor(number / 1000000000),
        number,
        true,
      )} ${billion_w} ${numberInWords}`;
    } else {
      numberInWords = `${get_1_999(Math.floor(number / 1000000000), number, true)} ${billion_w}`;
    }
  }

  const last_word = numberInWords.split(' ').slice(-1)[0];

  let new_last_word = '';
  let breakOccured = false;

  for (let i of range(10)) {
    if (last_word == numbers_and_words['numbers_words'][i]) {
      new_last_word = numbers_and_words['numbers_words3'][i];
      breakOccured = true;
    }
  }

  if (!breakOccured) {
    breakOccured = false;

    for (let i of range(big_number_words.length)) {
      if (last_word == big_number_words[i]) {
        new_last_word = big_number_words2[i];
        breakOccured = true;
      }
    }

    if (!breakOccured) {
      for (let i of range(tens_w.length)) {
        if (last_word == tens_w[i]) {
          new_last_word = tens_w2[i];
        }
      }
    }
  }

  const list = numberInWords.split(' ');

  list.pop();

  const result = list;
  result.push(new_last_word);

  return result.join(' ') + ' ';
};

const capitalize = (str) => {
  const firstLetter = str.substring(0, 1);
  const remainingLetters = str.substring(1);

  return `${firstLetter.toUpperCase()}${remainingLetters.toLowerCase()}`;
}

const run = number => {
  // Бүхэл хэсэг буюу 1200.18 гэхэд 1 мянга 2 зуун
  let inWords = numbersToWords(number);

  // ямар нэгэн бутархай хэсэггүй бол 1,200,000: 1 сая 200 мянга
  // гэж харагдаад байгааг мянган болгож засаж байна
  if (inWords.substr(inWords.length - 2) != 'н ') {
    inWords = inWords.slice(0, -1) + 'н';
  }

  // Эхний үсгийг том болгох
  return `${capitalize(inWords)} төгрөг`;
};

export default run;
