import ldap from 'ldapjs';

export const getOptions = () => {
  const { LDAP_SERVER_URL, LDAP_ROOT_DN, LDAP_ROOT_PASSWORD, LDAP_SEARCH_BASE } = process.env;

  return {
    url: LDAP_SERVER_URL,
    bindDN: LDAP_ROOT_DN,
    bindCredentials: LDAP_ROOT_PASSWORD,
    searchBase: LDAP_SEARCH_BASE,
    searchFilter: '(samaccountname={{username}})',
    searchAttributes: ['displayName', 'samaccountname'],
    reconnect: true,
  };
};

// try to get all users
export const checkLdap = (req, res) => {
  const options = getOptions();
  const adminClient = ldap.createClient(options);

  // connect as admin
  adminClient.bind(options.bindDN, options.bindCredentials, error => {
    if (error) {
      return res.end(error);
    }

    const users = [];

    // for for all users
    adminClient.search(
      options.searchBase,
      { scope: 'sub', filter: '(objectClass=user)' },
      (err, ldapResult) => {
        if (error) {
          return res.end(error);
        }

        // user found
        ldapResult.on('searchEntry', entry => {
          users.push(entry.object);
        });

        // error ocurred
        ldapResult.on('error', err => {
          return res.json(err);
        });

        // after receving all users send response
        ldapResult.on('end', () => {
          return res.json(users);
        });
      },
    );
  });

  // handling ldap un caught exceptions
  process.on('uncaughtException', e => {
    if (!res.headersSent) {
      res.json(e);
    }
  });
};

// find user
export const findUser = (samaccountname, callback) => {
  const options = getOptions();
  const adminClient = ldap.createClient(options);

  // connect as admin
  adminClient.bind(options.bindDN, options.bindCredentials, error => {
    if (error) {
      callback({ status: 'error', error });
    }

    // for for all users
    adminClient.search(
      options.searchBase,
      { scope: 'sub', filter: `(samaccountname=${samaccountname})` },
      (err, ldapResult) => {
        if (error) {
          callback({ status: 'error', error });
        }

        let user;

        // user found
        ldapResult.on('searchEntry', entry => {
          user = entry.object;
        });

        ldapResult.on('end', () => {
          if (user) {
            return callback({ status: 'ok', user });
          }

          callback({ status: 'error', error: { message: 'Хэрэглэгч олдсонгүй' } });
        });
      },
    );
  });

  // handling ldap un caught exceptions
  process.on('uncaughtException', error => {
    callback({ status: 'error', error });
  });
};
