export const getQueryParam = (name) => {
  const url = window.location.href;

  name = name.replace(/[[\]]/g, '\\$&');

  const regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
    results = regex.exec(url);

  if (!results) return null;

  if (!results[2]) return '';

  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

export const setQueryParam = (key, val) => {
  const url = window.location.href;
  const regex = RegExp(`([?&]${key}(?=[=&#]|$)[^#&]*|(?=#|$))`);

  window.location.href = url
    .replace(regex, `&${key}=${encodeURIComponent(val)}`)
    .replace(/^([^?&]+)&/, '$1?');
}
