import Pagination from './pagination';
import { setQueryParam, getQueryParam } from './common';

export class IncomeList {
  constructor() {
    this.searchFields = [
      'billNumber', 'accountNumber', 'from', 'groupCode', 'amount', 'date',
      'createdUserId', 'accountHolder'
    ];

    this.handleEvents();
    this.initSearchFields();
    this.initSortFields();
    this.renderPagination();
    this.toggleClearHandler();
  }

  toggleClearHandler() {
    $('#clear-search').toggle(Boolean(window.location.search));
  }

  handleEvents() {
    $('input[name="date"]').datepicker();

    // do search
    $('.income-search .search').change((e) => {
      const name = $(e.target).attr('name');
      const value = e.target.value;

      setQueryParam(name, value);
    });

    // do sort
    $('.income-search input[type="radio"]').change((e) => {
      const name = $(e.target).attr('id').replace('sort-', '');

      setQueryParam('sortField', name);
    });

    // clear search
    $('#clear-search').click(() => {
      window.location.href = '/';
    });

    // do print
    $('#print').click(() => {
      const status = window.location.pathname.includes('deleted') ? '-1' : '1';
      window.open(`/incomes/print/${status}/${window.location.search}`);
    });
  }

  renderPagination() {
    const pagination = new Pagination({ count: incomesCount }); // eslint-disable-line

    $('#pagination').html(pagination.render());
  }

  initSearchFields() {
    // fill search input values from url params
    this.searchFields.forEach((field) => {
      let value = getQueryParam(field);

      $(`[name="${field}"]`).val(value);
    });
  }

  initSortFields() {
    const value = getQueryParam('sortField') || 'billNumber';

    $(`input[id="sort-${value}"]`).attr('checked', 'checked');
  }
}

new IncomeList();
