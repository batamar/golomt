export class IncomeForm {
  constructor(initialRecords) {
    this.records = initialRecords;

    this.handleEvents();
    this.handleShortcuts();
  }

  handleEvents() {
    // remove record
    $(document).on('click', '.remove-record', (e) =>
      this.removeRecord($(e.target).attr('group-code'))
    );

    // accounts loader
    $('input[name="groupCode"]').keydown((e) => {
      if (e.keyCode === 13) {
        e.preventDefault();

        // fetch accounts by group code
        this.loadAccounts(e.target.value);

        return false;
      }
    });

    $('input[name="from"]').keydown((e) => {
      if (e.keyCode === 13) {
        e.preventDefault();

        $('input[name="groupCode"]').focus();

        return false;
      }
    });

    $('.dismiss').click(() => {
      window.location.href = '/';
    });

    // form submit
    $('form').submit((e) => {
      this.formSubmit(e);
    });
  }

  handleShortcuts() {
    $(document).keydown((e) => {
      switch (e.keyCode) {
        // f4
        case 115:
          window.location.href = '/';
          break;

        // f5
        case 116:
          e.preventDefault();
          break;

        // f7
        case 118:
          $('form').submit();
          break;

        // f9
        case 120:
          fetch('/incomes/load-last-income', { credentials: 'include' })
            .then(res => res.json())
            .then((income) => {
              $('input[name="from"]').val(income.from);
              $('input[name="groupCode"]').focus();
            })
            .catch((e) => {
              console.log(e) // eslint-disable-line
            })
          break;

        default:
      }
    })
  }

  formSubmit(e) {
    e.preventDefault();

    // if there is no record then do nothing
    if (this.records.length === 0) {
      return false;
    }

    const updatedRecords = [];

    // calculate each record's amounts from input
    for (let record of this.records) {
      updatedRecords.push({
        ...record,
        amount: parseInt($(`#amount-${record._id}`).val()),
      });
    }

    // disable submit
    $('[type="submit"]').attr('disabled', 'disabled');

    fetch('/incomes/create', {
      method: 'post',
      headers: {'Content-Type':'application/x-www-form-urlencoded'},
      credentials: 'include',
      body: `records=${JSON.stringify(updatedRecords)}`
    })

    .then(response => response.json())

    .then(({ status, error, incomeId }) => {
      // enable submit
      $('[type="submit"]').removeAttr('disabled');

      if (status === 'ok') {
        window.open(`/incomes/document/${incomeId}`);

        window.location.href = '/incomes/create';
      }

      if (status === 'error') {
        alert(error);
      }
    })

    .catch((e) => {
      console.log(e) // eslint-disable-line
    });

    return false;
  }

  loadAccounts(code) {
    if (!code) {
      return false;
    }

    // if already exists then do not insert duplicated item
    if (this.records.find(record => record.groupCode === code)) {
      return false;
    }

    fetch(`/incomes/load-accounts/${code}`, { credentials: 'include' })
      .then(res => res.json())
      .then((accounts) => {
        this.addRecord(accounts || []);

        // clear group code value after load records
        $('[name="groupCode"]').val('');
      })
      .catch((e) => {
        console.log(e) // eslint-disable-line
      })
  }

  addRecord(accounts) {
    const { status, doc, message } = this.getFormValues();

    if (status === 'error') {
      return alert(message);
    }

    accounts.forEach(account => {
      this.records.push({
        ...account,
        ...doc,
      });
    });

    this.renderRecords();

    return 'success';
  }

  /*
   * validate form or return doc
   */
  getFormValues() {
    const groupCode = $('[name="groupCode"]').val();
    const from = $('[name="from"]').val();
    const recipientId = $('[name="recipientId"]').val();

    if (!groupCode) {
      return {
        status: 'error',
        message: 'Зориулалтын төрөл сонгоно уу',
      };
    }

    if (!from) {
      return {
        status: 'error',
        message: 'Хэнээс талбарын мэдээллийг бөглөнө үү',
      };
    }

    if (!recipientId) {
      return {
        status: 'error',
        message: 'Хүлээн авагчийн мэдээллийг бөглөнө үү',
      };
    }

    // form is valid then return doc
    return {
      status: 'ok',
      doc:{
        billNumber: $('[name="billNumber"]').val(),
        from,
        recipientId,
        groupCode,
      },
    };
  }

  removeRecord(groupCode) {
    this.records = this.records.filter(record => record.groupCode !== groupCode)
    this.renderRecords();
  }

  calculateTotal() {
    let total = 0;

    $('input.amount').each(function () {
      total += parseInt($(this).val());
    });

    $('#total-amount').html(total.toLocaleString());
  }

  renderRecords() {
    // clear previous data
    $('#records tbody').html('');

    this.records.forEach((record) => {
      $('#records tbody').append(
        `
        <tr>
          <td>
            ${record.billNumber}
          </td>
          <td>
            ${record.from}
          </td>
          <td>
            ${record.groupCode}
          </td>
          <td>
            ${record.dedicationType.name}
          </td>
          <td>
            ${record.accountNumber}
          </td>
          <td>
            <input class='amount' id='amount-${record._id}' value='${record.amount}' />
          </td>
          <td>
            <button
              type='button'
              class='remove-record btn btn-xs btn-danger'
              group-code='${record.groupCode}'>
              x
            </button>
          </td>
        </tr>
        `
      );

      // display with thousands seperator
      $('#records tbody tr:last-child .amount').divide();

      $('#records tbody tr:last-child .amount').keyup(() => {
        this.calculateTotal();
      });
    });

    this.calculateTotal();
  }
}

new IncomeForm([]);
