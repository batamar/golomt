import { setQueryParam, getQueryParam } from './common';
import { PER_PAGE } from '../../constants';

// pages calculation
const generatePages = (pageCount, currentPage) => {
  const w = 4;

  let pages = _.range(1, pageCount + 1);
  let diff;
  let first = pages.slice(0, w);

  const last = pages.slice(-w);

  let currentStart = currentPage - 1 - w;

  if (currentStart < 0) {
    currentStart = 0;
  }

  let currentEnd = currentPage - 1 + w;

  if (currentEnd < 0) {
    currentEnd = 0;
  }

  const current = pages.slice(currentStart, currentEnd);

  pages = [];

  if (_.intersection(first, current).length === 0) {
    pages = pages.concat(first);
    diff = current[0] - _.last(first);

    if (diff === 2) {
      pages.push(current[0] - 1);
    } else if (diff !== 1) {
      pages.push(null);
    }

    pages = pages.concat(current);
  } else {
    pages = _.union(first, current);
  }

  if (_.intersection(current, last).length === 0) {
    diff = last[0] - _.last(pages);

    if (diff === 2) {
      pages.push(last[0] - 1);
    } else if (diff !== 1) {
      pages.push(null);
    }

    pages = pages.concat(last);
  } else {
    diff = _.difference(last, current);
    pages = pages.concat(diff);
  }

  return pages;
}

// change url page parameter
const goto = (page) => {
  setQueryParam('page', page);
}

// per page
class Page {
  constructor (props) {
    this.props = props;

    this.handleEvents();
  }

  handleEvents() {
    $(document).on('click', `[key="page-${this.props.key}"]`, () => {
      goto(this.props.page);
    });
  }

  render () {
    const { key, currentPage, page } = this.props;

    if (page) {
      let className = '';

      if (page === currentPage) {
        className += ' active disabled';
      }

      return `
        <li class="${className}" key="page-${key}">
          <a href='#'>${page}</a>
        </li>
      `;
    }

    return `
      <li class='disabled'>
        <span>...</span>
      </li>
    `;
  }
}

export default class Pagination {
  constructor ({ count }) {
    const currentPage = Number(getQueryParam('page') || 1);
    const perPage = Number(getQueryParam('perPage') || PER_PAGE);

    let totalPagesCount = parseInt(count / perPage) + 1

    if (count % perPage === 0) {
      totalPagesCount -= 1;
    }

    // calculate page numbers
    const pages = generatePages(totalPagesCount, currentPage)

    this.props = {
      currentPage,
      isPaginated: totalPagesCount > 1,
      totalPagesCount,
      pages,
    };

    this.handleEvents();
  }

  handleEvents() {
    const { totalPagesCount, currentPage } = this.props;

    $(document).on('click', 'a.prev', (e) => {
      e.preventDefault();

      const page = currentPage - 1;

      if (page > 0) {
        goto(page);
      }
    });

    $(document).on('click', 'a.next', (e) => {
      e.preventDefault();

      const page = currentPage + 1;

      if (page <= totalPagesCount) {
        goto(page);
      }
    });
  }

  renderBar () {
    const { totalPagesCount, pages, currentPage, isPaginated } = this.props;

    if (isPaginated) {
      let prevClass = '';
      let nextClass = '';

      if (currentPage <= 1) {
        prevClass = 'disabled';
      }

      if (currentPage >= totalPagesCount) {
        nextClass = 'disabled';
      }

      let renderedPages = '';

      pages.forEach((page, index) => {
        const pageObject = new Page({ currentPage, page, key: index });

        renderedPages += pageObject.render();
      });

      return `
        <nav>
          <ul class='pagination'>
            <li>
              <a href='' class="${prevClass} prev"><</a>
            </li>

            ${renderedPages}

            <li>
              <a href='#' class="${nextClass} next">></a>
            </li>
          </ul>
        </nav>
      `
    }

    return '';
  }

  render () {
    return `
      <div class='pagination-container'>
        <div class='pull-right'>
          ${this.renderBar()}
        </div>

        <div class='clearfix' />
      </div>
    `
  }
}
