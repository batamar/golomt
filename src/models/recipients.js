import mongoose from 'mongoose';
import { Departments, Incomes } from './';

const RecipientSchema = mongoose.Schema({
  departmentId: {
    type: String,
    required: true,
  },

  name: {
    type: String,
    required: true,
  },

  code: {
    type: String,
    required: true,
  },

  logo: {
    type: String,
  },

  isActive: {
    type: Boolean,
  },

  // auto fields
  createdDate: {
    type: Date,
  },
});

class Recipient {
  static createMethod(doc) {
    doc.createdDate = new Date();
    return this.create(doc);
  }

  static updateMethod(_id, doc) {
    return this.findByIdAndUpdate(_id, { $set: doc });
  }

  static async deleteMethod(_id) {
    const income = await Incomes.findOne({ recipientId: _id });

    // used in income
    if (income) {
      throw new Error(`${income.billNumber} орлогод ашигласан байгаа тул устгах боломжгүй байна`);
    }

    const department = await Departments.findOne({ defaultRecipientId: _id });

    // used in department
    if (department) {
      throw new Error(`${department.name} салбарт ашигласан байгаа тул устгах боломжгүй байна`);
    }

    return this.remove({ _id });
  }
}

RecipientSchema.loadClass(Recipient);

const Recipients = mongoose.model('recipients', RecipientSchema);

export default Recipients;
