import mongoose from 'mongoose';
import moment from 'moment';
import { Departments } from './';

const IncomeSchema = mongoose.Schema({
  // Бүх баримтанд байх автомат дугаар
  number: {
    type: Number,
    required: true,
  },

  // Өдөр болгон 1 ээс эхлэх 1 хэрэглэгийн бүртгүүлсэн олон орлогын
  // дунд 1 байх дугаар
  billNumber: {
    type: Number,
    required: true,
  },

  from: {
    type: String,
    required: true,
  },

  groupCode: {
    type: String,
    required: true,
  },

  dedicationTypeId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },

  amount: {
    type: Number,
    required: true,
  },

  accountNumber: {
    type: Number,
    required: true,
  },

  accountHolder: {
    type: String,
  },

  departmentId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },

  recipientId: {
    type: String,
    required: true,
  },

  // will determine whether print in seperate paper
  printSeparateDocument: {
    type: Boolean,
    default: false,
  },

  // auto fields ================
  createdUserId: {
    type: mongoose.Schema.Types.ObjectId,
  },

  createdDate: {
    type: Date,
  },

  // 1 = active
  // -1 = deleted
  status: {
    type: Number,
    required: true,
  },

  // TODO remove
  oldNumberOfDocumentPrints: Number,
  numberOfDocumentPrints: {
    type: Number,
    required: true,
  },

  deletedReason: {
    type: String,
  },

  deletedDate: {
    type: Date,
  },
});

let billNumbers = {};
let lastRecordNumber = 0;

class Income {
  static getInformation() {
    console.log('info ..........'); // eslint-disable-line
    console.log(billNumbers, lastRecordNumber); // eslint-disable-line
    return { billNumbers, lastRecordNumber }
  }

  static async calculateRuntimeVariables() {
    console.log('started calculating ..........'); // eslint-disable-line

    // reset
    billNumbers = {};
    lastRecordNumber = 0;

    // caluculate last record number ================
    const lastIncomes = await this.find().sort({ number: -1 });

    if (lastIncomes.length > 0) {
      lastRecordNumber = lastIncomes[0].number;
    }

    // calculate last bill numbers ===================
    const departments = await Departments.find({});
    const startOfDay = moment().startOf('day');
    const nextDay = moment(startOfDay).add(1, 'days');

    for(let department of departments) {
      const departmentId = department._id;

      // filter incomes created only today
      const incomes = await this.find({
        departmentId,
        createdDate: {
          $gte: startOfDay,
          $lt: nextDay,
        },
      })
        // sort by created date
        .sort({
          createdDate: -1,
        });

      let billNumber = 0;

      // get last one
      if (incomes.length > 0) {
        const lastIncome = incomes[0];

        billNumber = lastIncome.billNumber;
      }

      billNumbers[this.billNumberKey(departmentId)] = billNumber;
    }
  }

  static billNumberKey(departmentId) {
    const now = new Date();

    return `${departmentId}-${now.toDateString()}`;
  }

  /*
   * Return last bill number
   */
  static getLastBillNumber(departmentId) {
    const key = this.billNumberKey(departmentId);

    // get last bill number from runtime variable
    return billNumbers[key] || 0;
  }

  /*
   * Only will generate, will not set runtime variable
   */
  static getNextBillNumber(departmentId) {
    // get last bill number from runtime variable
    const lastBillNumber = this.getLastBillNumber(departmentId);

    return lastBillNumber + 1;
  }

  /*
   * Will set runtime variable
   */
  static generateBillNumber(departmentId) {
    let lastBillNumber = this.getLastBillNumber(departmentId);

    // increment it
    lastBillNumber++;

    // save back
    billNumbers[this.billNumberKey(departmentId)] = lastBillNumber;

    // return updated value
    return lastBillNumber;
  }

  /*
   * Increment lastRecorNumber runtime variable value
   */
  static generateRecordNumber() {
    lastRecordNumber++;

    return lastRecordNumber;
  }

  static async createMethod(records, user) {
    const startOfDay = moment().startOf('day');
    const nextDay = moment(startOfDay).add(1, 'days');

    // to become all record have same date creating createdDate here at once
    const createdDate = new Date();
    const departmentId = user.departmentId;

    // checking that exactly same entries are inserted before
    for(let record of records) {
      const sameEntry = await this.findOne({
        from: record.from,
        groupCode: record.groupCode,
        dedicationTypeId: record.dedicationTypeId,
        amount: record.amount,

        departmentId,
        createdUserId: user._id,

        // today
        createdDate: {
          $gte: startOfDay,
          $lt: nextDay,
        },
      });

      if (sameEntry) {
        throw new Error(
          `Хэнээс: ${record.from}, Зориулалт: ${record.groupCode}, Дүн: ${record.amount}, Бүхий мэдээлэл давхардсан байна.` // eslint-disable-line
        );
      }
    }

    // get this user's lastly created income
    const lastDbIncome = await this.findOne({
      createdUserId: user._id
    }).sort({
      createdDate: -1
    });

    // do not save repeated income
    if (lastDbIncome && (createdDate - lastDbIncome.createdDate) < 3000) {
      return 'too often save';
    }

    // check billNumber duplications =======================
    // filter incomes created only today
    const duplicatedEntries = await this.find({
      departmentId,
      billNumber: this.getNextBillNumber(departmentId),
      createdDate: {
        $gte: startOfDay,
        $lt: nextDay,
      },
    });

    if (duplicatedEntries.length > 0) {
      return 'duplicated bill number';
    }

    // generating billNumber ======
    const billNumber = this.generateBillNumber(departmentId);

    let lastIncome;

    for(let record of records) {
      delete record._id;

      const number = this.generateRecordNumber();

      // check duplication
      if (await this.find({ number }).count() > 0) {
        return 'duplicated record number';
      }

      const doc = {
        ...record,
        createdDate: createdDate,
        createdUserId: user._id,
        status: 1,
        numberOfDocumentPrints: 1,
        billNumber,
        number,
      }

      lastIncome = await this.create(doc);
    }

    return lastIncome;
  }

  static async deleteMethod({ _id, reason, user }) {
    const income = await this.findOne({ _id });
    const createdDate = income.createdDate;
    const now = new Date();
    const yesterday = moment(now).subtract('1', 'days');

    // only admin can delete previous day's incomes
    if (user.role !== 'admin' && moment(createdDate).isBefore(yesterday)) {
      throw new Error('Өмнөх өдрийн гүйлгээг зөвхөн админ устгах боломжтой');
    }

    return this.update(
      { createdDate: income.createdDate },
      { $set: { status: -1, deletedReason: reason, deletedDate: now } },
      { multi: true },
    );
  }

  static async increaseDocumentPrintedNumber(incomeId) {
    const income = await Incomes.findOne({ _id: incomeId });

    return this.update(
      {
        createdDate: income.createdDate,
      },
      { $inc: { numberOfDocumentPrints: 1 } },
      { multi: true }
    );
  }
}

IncomeSchema.loadClass(Income);

const Incomes = mongoose.model('income', IncomeSchema);

export default Incomes;
