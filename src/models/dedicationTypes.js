import mongoose from 'mongoose';
import { Incomes, Accounts } from './';

const DedicationTypeSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
  },

  isActive: {
    type: Boolean,
  },

  // auto fields ====================
  createdDate: {
    type: Date,
  },
});

class DedicationType {
  static createMethod(doc) {
    doc.createdDate = new Date();
    return this.create(doc);
  }

  static updateMethod(_id, doc) {
    return this.findByIdAndUpdate(_id, { $set: doc });
  }

  static async deleteMethod(_id) {
    const account = await Accounts.findOne({ dedicationTypeId: _id });

    const income = await Incomes.findOne({ dedicationTypeId: _id });

    // used in account
    if (account) {
      throw new Error(
        `${account.accountNumber} харгалзаанд ашигласан байгаа тул устгах боломжгүй байна`,
      );
    }

    // used in income
    if (income) {
      throw new Error(
        `${income.billNumber} орлогод ашигласан байгаа тул устгах боломжгүй байна`,
      );
    }

    return this.remove({ _id });
  }
}

DedicationTypeSchema.loadClass(DedicationType);

const DedicationTypes = mongoose.model('dedication_types', DedicationTypeSchema);

export default DedicationTypes;
