import mongoose from 'mongoose';
import { generateShortName } from '../helpers/auth';
import { Incomes } from './';

const UserSchema = mongoose.Schema({
  sAMAccountName: {
    type: String,
    required: true,
  },

  departmentId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },

  role: {
    type: String,
    required: true,
    choices: ['inactive', 'admin', 'supervisor', 'teller'],
  },

  firstName: {
    type: String,
  },

  lastName: {
    type: String,
  },
});

UserSchema.methods = {
  shortName() {
    return generateShortName(this);
  }
}

class User {
  static async createMethod(doc) {
    const user = await this.findOne({ sAMAccountName: doc.sAMAccountName });

    if (user) {
      throw new Error('Ийм нэртэй хэрэглэгч аль хэдийнэ бүртгэгдсэн байна');
    }

    return this.create(doc);
  }

  static async updateMethod(_id, doc) {
    const user = await this.findOne({ sAMAccountName: doc.sAMAccountName });

    if (user && user._id.toString() !== _id) {
      throw new Error('Ийм нэртэй хэрэглэгч аль хэдийнэ бүртгэгдсэн байна');
    }

    return this.findByIdAndUpdate(_id, { $set: doc });
  }

  static async deleteMethod(_id) {
    // used in income
    const income = await Incomes.findOne({ createdUserId: _id });

    if (income) {
      throw new Error('Уг хэрэглэгч орлого үүсгэсэн байгаа тул устгах боломжгүй байна');
    }

    return this.remove({ _id });
  }
}

UserSchema.loadClass(User);

const Users = mongoose.model('users', UserSchema);

export default Users;
