import faker from 'faker';
import { DedicationTypes, Departments, Users, Incomes } from './';

export const dedicationTypeFactory = (params = {}) => {
  const dedicationType = new DedicationTypes({
    name: params.name || faker.random.word(),
    isActive: params.isActive || true,
    createdDate: params.createdDate || new Date(),
  });

  return dedicationType.save();
}

export const departmentFactory = (params = {}) => {
  const department = new Departments({
    name: params.name || faker.random.word(),
    defaultRecipientId: params.defaultRecipientId || 'DFFDSFADSFDF',
    documentType: params.documentType || 1,
    documentCopies: params.documentCopies || 1,
  });

  return department.save();
}

export const userFactory = async (params = {}) => {
  const department = await departmentFactory();

  const user = new Users({
    sAMAccountName: params.sAMAccountName || faker.random.word(),
    role: params.role || 'teller',
    firstName: params.firstName || 'First name',
    lastName: params.lastName || 'Last name',
    departmentId: params.departmentId || department._id,
  });

  return user.save();
}

export const incomeFactory = async (params = {}) => {
  const user = await userFactory();

  const income = new Incomes({
    number: params.number || faker.random.number(),
    billNumber: params.billNumber || faker.random.number(),
    createdUserId: params.createdUserId || user._id,
    createdDate: params.createdDate || new Date(),
    status: params.status || 1,
    numberOfDocumentPrints: params.numberOfDocumentPrints || 1,

    ...recordFactory(),
  });

  return income.save();
};

export const recordFactory = async (params = {}) => {
  const dedicationType = await dedicationTypeFactory();

  return {
    from: params.from || 'from',
    groupCode: params.groupCode || faker.random.word(),
    dedicationTypeId: params.dedicationTypeId || dedicationType._id,
    amount: params.amount || faker.random.number(),
    accountNumber: params.accountNumber || faker.random.number(),
    accountHolder: params.accountHolder || faker.random.word(),
    departmentId: params.departmentId,
    recipientId: params.recipientId || 'recipient',
    printSeparateDocument: params.printSeparateDocument || false,
  };
};
