import Users from './users';
import Departments from './departments';
import DedicationTypes from './dedicationTypes';
import Recipients from './recipients';
import Accounts from './accounts';
import Incomes from './incomes';

export { Users, Departments, DedicationTypes, Recipients, Incomes, Accounts };
