import mongoose from 'mongoose';
import { Accounts } from './';

const DepartmentSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
  },

  defaultRecipientId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },

  // 0 - single row
  // 1 - multiple row
  documentType: {
    type: Number,
    required: true,
  },

  documentCopies: {
    type: Number,
    required: true,
  },
});

class Department {
  static createMethod(doc) {
    return this.create(doc);
  }

  static updateMethod(_id, doc) {
    return this.findByIdAndUpdate(_id, { $set: doc });
  }

  static async deleteMethod(_id) {
    // used in account
    const account = await Accounts.findOne({ departmentId: _id });

    if (account) {
      throw new Error(
        `${account.accountNumber} харгалзаанд ашигласан байгаа тул устгах боломжгүй байна`,
      );
    }

    return this.remove({ _id });
  }
}

DepartmentSchema.loadClass(Department);

const Departments = mongoose.model('departments', DepartmentSchema);

export default Departments;
