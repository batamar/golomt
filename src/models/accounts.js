import mongoose from 'mongoose';

const AccountSchema = mongoose.Schema({
  groupCode: {
    type: Number,
    required: true,
  },

  accountNumber: {
    type: Number,
    required: true,
  },

  accountHolder: {
    type: String,
  },

  dedicationTypeId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },

  departmentId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },

  amount: {
    type: Number,
    required: true,
  },

  // will determine whether print in seperate paper
  printSeparateDocument: {
    type: Boolean,
    default: false,
  },

  // auto fields ====================
  createdDate: {
    type: Date,
  },
});

class Account {
  static createMethod(doc) {
    doc.createdDate = new Date();
    return this.create(doc);
  }

  static updateMethod(_id, doc) {
    doc.printSeparateDocument = doc.printSeparateDocument || false;
    return this.findByIdAndUpdate(_id, { $set: doc });
  }

  static deleteMethod(_id) {
    return this.remove({ _id });
  }
}

AccountSchema.loadClass(Account);

const Accounts = mongoose.model('accounts', AccountSchema);

export default Accounts;
